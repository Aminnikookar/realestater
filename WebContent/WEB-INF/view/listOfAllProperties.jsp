<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>


<head>
<title>List Customers</title>

<!-- reference our style sheet -->

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>


<body>

	<div id="wrapper">
		<div id="header">
			<h2>RealEstater Manager</h2>
			<h3>Here you can see all your tables and delete or update a
				property is available</h3>
			<br /> <br />
		</div>
	</div>



	<!-- a link to add a new property -->

	<div id="container">
		<div id="content">

			<input type="button" value="Add a new property"
				onclick="window.location.href='console'; return false;"
				class="add-button" /> <br /> <br />

		</div>

	</div>



	<div id="container">
		<div id="content">

			<!-- Add our html table here for residentials -->

			<h3>Residentials Properties</h3>

			<table>
				<tr>
					<th>Title</th>
					<th>Image</th>
					<th>Building Type</th>
					<th>Price</th>
					<th>Sale/Rent</th>
					<th>Summary</th>
					<th>Rooms</th>
					<th>Bathrooms</th>
					<th>Date of Publish</th>
					<th>Address</th>
					<th>Postal Code</th>
					<th>City</th>
					<th>Update/Delete</th>

				</tr>


				<!-- Loop over and print our properties -->
				<!-- this properties below is the one we named in model -->

				<c:forEach var="tempResidential" items="${residential}">


					<!-- construct an "update" link with property id -->
					<c:url var="updateLink" value="/residential/showFormForUpdate">
						<c:param name="propertyId" value="${tempResidential.id}" />
					</c:url>


					<!-- construct a "delete" link with property id -->
					<c:url var="deleteLink" value="/residential/deleteResidential">
						<c:param name="propertyId" value="${tempResidential.id}" />
					</c:url>


					<tr>
						<td>${tempResidential.title}</td>
						<td>${tempResidential.image}</td>
						<td>${tempResidential.buildingType}</td>
						<td>${tempResidential.price}</td>
						<td>${tempResidential.saleOrRent}</td>
						<td>${tempResidential.summary}</td>
						<td>${tempResidential.rooms}</td>
						<td>${tempResidential.bathrooms}</td>
						<td>${tempResidential.dateOfPublish}</td>
						<td>${tempResidential.address}</td>
						<td>${tempResidential.postalCode}</td>
						<td>${tempResidential.city}</td>

						<td>
							<!-- display the update and delete link --> <a
							href="${updateLink}">Update</a> | <a href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want to delete this property?'))) return false">Delete</a>
						</td>

					</tr>

				</c:forEach>

			</table>

		</div>

	</div>
	<!-- ------------------------------------------------------------------------------------------ -->



	<div id="container">
		<div id="content">

			<!-- Add our html table here for recreationals -->

			<h3>Recreational Properties</h3>

			<table>
				<tr>
					<th>Title</th>
					<th>Image</th>
					<th>Building Type</th>
					<th>Price</th>
					<th>Sale/Rent</th>
					<th>Summary</th>
					<th>Rooms</th>
					<th>Bathrooms</th>
					<th>Date of Publish</th>
					<th>Address</th>
					<th>Postal Code</th>
					<th>City</th>
					<th>Update/Delete</th>

				</tr>


				<!-- Loop over and print our properties -->
				<!-- this properties below is the one we named in model -->

				<c:forEach var="tempRecreational" items="${recreational}">

					<!-- construct an "update" link with property id -->
					<c:url var="updateLink" value="/residential/showFormForUpdateRecreational">
						<c:param name="propertyId" value="${tempRecreational.id}" />
					</c:url>


					<!-- construct a "delete" link with property id -->
					<c:url var="deleteLink" value="/residential/deleteRecreational">
						<c:param name="propertyId" value="${tempRecreational.id}" />
					</c:url>


					<tr>
						<td>${tempRecreational.title}</td>
						<td>${tempRecreational.image}</td>
						<td>${tempRecreational.buildingType}</td>
						<td>${tempRecreational.price}</td>
						<td>${tempRecreational.saleOrRent}</td>
						<td>${tempRecreational.summary}</td>
						<td>${tempRecreational.rooms}</td>
						<td>${tempRecreational.bathrooms}</td>
						<td>${tempRecreational.dateOfPublish}</td>
						<td>${tempRecreational.address}</td>
						<td>${tempRecreational.postalCode}</td>
						<td>${tempRecreational.city}</td>

						<td>
							<!-- display the update and delete link --> <a
							href="${updateLink}">Update</a> | <a href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want to delete this property?'))) return false">Delete</a>
						</td>


					</tr>

				</c:forEach>

			</table>

		</div>

	</div>

	<!-- ------------------------------------------------------------------------------------------ -->

</body>

</html>