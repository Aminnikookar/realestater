<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/add-customer-style.css" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<body>
	<div id="wrapper">
		<div id="header">
			<h2>Console to enter the property specifications</h2>

		</div>
	</div>
	<div id="container">

		<label class="labelblock1">Upload images you want to put on
			server : </label>

		<form action="upload" method="post" enctype="multipart/form-data">
			<input type="file" name="file" /> <br /> <br /> <input
				type="submit" value="send" />
		</form>
		<br />


		<form:form action="saveProperty" modelAttribute="residential"
			method="POST">

			<form:hidden path="id" />


			<table style="width: 100%">
				<tbody>


					<tr>
						<td width="50%"><label id="label">Choose the type of
								the property : </label></td>
						<td width="50%"><form:select path="options">
								<option value="residentialOption" label="Residential" />
								<option value="recreationalOption" label="Recreational" />
								<option value="CondoOption" label="Condo" />
								<option value="MultiFamilyOption" label="Multi Family" />
								<option value="AgricultureOption" label="Agriculture" />
								<option value="VacantLandOption" label="Vacant Land" />
								<option value="NoPreferenceOption" label="No Preference" />
								<option selected="selected">${residential.options}</option>
							</form:select></td>
					</tr>



					<tr>
						<td width="50%"><label>Is this for sale or rent ? </label></td>
						<td width="50%"><form:select path="saleOrRent">

								<option value="sale" label="For Sale" />
								<option value="rent" label="For Rent" />
								<option value="saleOrRent" label="For Sale or Rent" />
								<option selected="selected">${residential.saleOrRent}</option>

							</form:select></td>
					</tr>



					<tr>
						<td><label>Choose how many rooms is available ? </label></td>
						<td><form:select path="rooms">

								<option value="5" label="Rooms" />
								<option value="1" label="1" />
								<option value="2" label="2" />
								<option value="3" label="3" />
								<option value="4" label="4" />
								<option value="5" label="5" />
								<option selected="selected">${residential.rooms}</option>

							</form:select></td>
					</tr>



					<tr>
						<td><label>Choose how many bathrooms is available ? </label></td>
						<td><form:select path="bathrooms">

								<option value="5" label="Bathrooms" />
								<option value="1" label="1" />
								<option value="2" label="2" />
								<option value="3" label="3" />
								<option value="4" label="4" />
								<option value="5" label="5" />
								<option selected="selected">${residential.bathrooms}</option>

							</form:select></td>
					</tr>


					<tr>
						<td><label>Enter the building type : </label></td>
						<td><form:textarea path="buildingType" /></td>
					</tr>

					<tr>
						<td><label>Enter the price : </label></td>
						<td><form:input path="price" /></td>
					</tr>



					<tr>
						<td><label>Enter the title : </label></td>
						<td><form:textarea path="title" /></td>
					</tr>

					<tr>
						<td><label>Enter the address : </label></td>
						<td><form:textarea path="address" /></td>
					</tr>


					<tr>
						<td><label>Enter the postal code : </label></td>
						<td><form:textarea path="postalCode" /></td>
					</tr>


					<tr>
						<td><label>Enter the city : </label></td>
						<td><form:textarea path="city" /></td>
					</tr>


					<tr>
						<td><label>Enter the date of publish : </label></td>

			
						<td><fmt:formatDate value="${residential.dateOfPublish}"
								pattern="yyyy-MM-dd" var="myDate" /> <form:input
								path="dateOfPublish" value="${myDate}" /></td>
					</tr>

					<tr>
						<td><label>enter the image : </label></td>
						<td><form:input type="file" path="image" /></td>
					</tr>
					<tr>
						<td><label>Enter the description : <br /></label></td>

						<td><form:textarea id="summernote" path="summary" /></td>
					</tr>


					<tr>
						<td><label></label></td>
						<td><input type="submit" value="save" /></td>
					</tr>
				</tbody>
			</table>
		</form:form>
	</div>




	<script>
		$('#summernote').summernote(
				{
					placeholder : 'Please insert your details here ',
					tabsize : 2,
					height : 100,
					toolbar : [
							// [groupName, [list of button]]
							[
									'style',
									[ 'bold', 'italic', 'underline', 'clear',
											'fontname' ] ],
							[
									'font',
									[ 'strikethrough', 'superscript',
											'subscript' ] ],
							[ 'fontsize', [ 'fontsize' ] ],
							[ 'color', [ 'color' ] ],
							[ 'para', [ 'ul', 'ol', 'paragraph' ] ],
							[ 'height', [ 'height' ] ],
							[ 'fullscreen', [ 'fullscreen' ] ],
							[ 'picture', [ 'picture' ] ],
							[ 'link', [ 'link' ] ], [ 'video', [ 'video' ] ],
							[ 'table', [ 'table' ] ], [ 'undo', [ 'undo' ] ],
							[ 'redo', [ 'redo' ] ], [ 'help', [ 'help' ] ] ],
					popover : {
						image : [
								[
										'imagesize',
										[ 'imageSize100', 'imageSize50',
												'imageSize25' ] ],
								[
										'float',
										[ 'floatLeft', 'floatRight',
												'floatNone' ] ],
								[ 'remove', [ 'removeMedia' ] ] ],
						link : [ [ 'link', [ 'linkDialogShow', 'unlink' ] ] ],
						air : [ [ 'color', [ 'color' ] ],
								[ 'font', [ 'bold', 'underline', 'clear' ] ],
								[ 'para', [ 'ul', 'paragraph' ] ],
								[ 'table', [ 'table' ] ],
								[ 'insert', [ 'link', 'picture' ] ] ]
					}
				});
	</script>
	<script>
		$(document).on('click', '#saveButton', function() {
			var detail = $('#summernote').summernote('code');

		});
	</script>
	<script type="text/javascript">
		var markupStr = $('#summernote').summernote('detail');
		$('#summernote').summernote('code', markupStr);
	</script>
</body>

</html>