<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>

<html>


<head>
<title>choosing</title>

<!-- reference our style sheet -->

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>


<body>

	<div id="wrapper">
		<div id="header">
			<h2>Choose what type of building are you looking for !</h2>
		</div>
	</div>

	<div>
	<p>
		<a href="${pageContext.request.contextPath}/residential/login">Login</a>
	</p>
	</div>

	<form:form action="process" modelAttribute="tableOption">

		<br>
		<br>

		<form:select path="options">

			<option value="residentialOption" label="Residential" />
			<option value="recreationalOption" label="Recreational" />
			<option value="CondoOption" label="Condo" />
			<option value="MultiFamilyOption" label="Multi Family" />
			<option value="AgricultureOption" label="Agriculture" />
			<option value="VacantLandOption" label="Vacant Land" />
			<option value="NoPreferenceOption" label="No Preference" />

		</form:select>



		<form:select path="forsaleOrRent">

			<option value="sale" label="For Sale" />
			<option value="rent" label="For Rent" />
			<option value="saleOrRent" label="For Sale or Rent" />

		</form:select>



		<form:select path="rooms">

			<option value="5" label="Rooms" />
			<option value="1" label="1 or more" />
			<option value="2" label="2 or more" />
			<option value="3" label="3 or more" />
			<option value="4" label="4 or more" />
			<option value="5" label="5 or more" />

		</form:select>



		<form:select path="bathrooms">

			<option value="5" label="Bathrooms" />
			<option value="1" label="1 or more" />
			<option value="2" label="2 or more" />
			<option value="3" label="3 or more" />
			<option value="4" label="4 or more" />
			<option value="5" label="5 or more" />

		</form:select>




		<br>
		<br>

		<form:select path="lowerPriceLimit">

			<option value="0" label="Lower Price Limit" />
			<option value="0" label="0" />
			<option value="25000" label="25,000" />
			<option value="50000" label="50,000" />
			<option value="75000" label="75,000" />
			<option value="100000" label="100,000" />
			<option value="125000" label="125,000" />
			<option value="150000" label="150,000" />
			<option value="175000" label="175,000" />
			<option value="200000" label="200,000" />
			<option value="225000" label="225,000" />
			<option value="250000" label="250,000" />
			<option value="275000" label="275,000" />
			<option value="300000" label="300,000" />
			<option value="325000" label="325,000" />
			<option value="350000" label="350,000" />
			<option value="375000" label="375,000" />
			<option value="400000" label="400,000" />
			<option value="425000" label="425,000" />
			<option value="450000" label="450,000" />
			<option value="475000" label="475,000" />
			<option value="500000" label="500,000" />
			<option value="550000" label="550,000" />
			<option value="600000" label="600,000" />
			<option value="650000" label="650,000" />
			<option value="700000" label="700,000" />
			<option value="750000" label="750,000" />
			<option value="800000" label="800,000" />
			<option value="850000" label="850,000" />
			<option value="900000" label="900,000" />
			<option value="950000" label="950,000" />
			<option value="1000000" label="1,000,000" />
			<option value="1100000" label="1,100,000" />
			<option value="1200000" label="1,200,000" />
			<option value="1300000" label="1,300,000" />
			<option value="1400000" label="1,400,000" />
			<option value="1500000" label="1,500,000" />
			<option value="1600000" label="1,600,000" />
			<option value="1700000" label="1,700,000" />
			<option value="1800000" label="1,800,000" />
			<option value="1900000" label="1,900,000" />
			<option value="2000000" label="2,000,000" />
			<option value="3000000" label="3,000,000" />
			<option value="4000000" label="4,000,000" />
			<option value="5000000" label="5,000,000" />
			<option value="7500000" label="7,500,000" />
			<option value="10000000" label="10,000,000" />

		</form:select>





		<form:select path="upperPriceLimit">

			<option value="0" label="Upper Price Limit" />
			<option value="0" label="0" />
			<option value="25000" label="25,000" />
			<option value="50000" label="50,000" />
			<option value="75000" label="75,000" />
			<option value="100000" label="100,000" />
			<option value="125000" label="125,000" />
			<option value="150000" label="150,000" />
			<option value="175000" label="175,000" />
			<option value="200000" label="200,000" />
			<option value="225000" label="225,000" />
			<option value="250000" label="250,000" />
			<option value="275000" label="275,000" />
			<option value="300000" label="300,000" />
			<option value="325000" label="325,000" />
			<option value="350000" label="350,000" />
			<option value="375000" label="375,000" />
			<option value="400000" label="400,000" />
			<option value="425000" label="425,000" />
			<option value="450000" label="450,000" />
			<option value="475000" label="475,000" />
			<option value="500000" label="500,000" />
			<option value="550000" label="550,000" />
			<option value="600000" label="600,000" />
			<option value="650000" label="650,000" />
			<option value="700000" label="700,000" />
			<option value="750000" label="750,000" />
			<option value="800000" label="800,000" />
			<option value="850000" label="850,000" />
			<option value="900000" label="900,000" />
			<option value="950000" label="950,000" />
			<option value="1000000" label="1,000,000" />
			<option value="1100000" label="1,100,000" />
			<option value="1200000" label="1,200,000" />
			<option value="1300000" label="1,300,000" />
			<option value="1400000" label="1,400,000" />
			<option value="1500000" label="1,500,000" />
			<option value="1600000" label="1,600,000" />
			<option value="1700000" label="1,700,000" />
			<option value="1800000" label="1,800,000" />
			<option value="1900000" label="1,900,000" />
			<option value="2000000" label="2,000,000" />
			<option value="3000000" label="3,000,000" />
			<option value="4000000" label="4,000,000" />
			<option value="5000000" label="5,000,000" />
			<option value="7500000" label="7,500,000" />
			<option value="10000000" label="10,000,000" />

		</form:select>

		<form:input path="dateOfPublish" type="date" value="Listed Since" />



		<br>
		<br>

		<input type="submit" value="Show results" class="add-button" />

	</form:form>


	<br>
	<br>



</body>

</html>