package com.luv2code.springdemo.entity;

import java.sql.Date;

public class TableOption {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String options;

	private String forsaleOrRent;

	private int lowerPriceLimit;

	private int upperPriceLimit;

	private int rooms;

	private int bathrooms;

	private Date dateOfPublish;

	private String saleOrRent;

	private String summary;

	private int price;

	private String title;

	private String detail;

	private String image;

	private String address;

	private String postalCode;

	private String city;

	private String buildingType;

	public String getBuildingType() {
		return buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSaleOrRent() {
		return saleOrRent;
	}

	public void setSaleOrRent(String saleOrRent) {
		this.saleOrRent = saleOrRent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getDateOfPublish() {
		return dateOfPublish;
	}

	public void setDateOfPublish(Date dateOfPublish) {
		this.dateOfPublish = dateOfPublish;
	}

	public int getRooms() {
		return rooms;
	}

	public void setRooms(int rooms) {
		this.rooms = rooms;
	}

	public int getBathrooms() {
		return bathrooms;
	}

	public void setBathrooms(int bathrooms) {
		this.bathrooms = bathrooms;
	}

	public int getLowerPriceLimit() {
		return lowerPriceLimit;
	}

	public int getUpperPriceLimit() {
		return upperPriceLimit;
	}

	public void setUpperPriceLimit(int upperPriceLimit) {
		this.upperPriceLimit = upperPriceLimit;
	}

	public void setLowerPriceLimit(int lowerPriceLimit) {
		this.lowerPriceLimit = lowerPriceLimit;
	}

	public String getOptions() {
		return options;
	}

	public String getForsaleOrRent() {
		return forsaleOrRent;
	}

	public void setForsaleOrRent(String forsaleOrRent) {
		this.forsaleOrRent = forsaleOrRent;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public TableOption() {

	}

}
