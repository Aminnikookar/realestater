package com.luv2code.springdemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class UserCheck {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="password")
    private String password;
	
	@Column(name="accessability")
	private String accessability;
	
   public UserCheck() {
	   setAccessability("this is null");
   }



public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getAccessability() {
	return accessability;
}

public void setAccessability(String accessability) {
	this.accessability = accessability;
}



@Override
public String toString() {
	return "User [id=" + id + ", name=" + name + ", password=" + password + ", accessability=" + accessability + "]";
}

}
