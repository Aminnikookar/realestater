package com.luv2code.springdemo.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.luv2code.springdemo.entity.Recreational;
import com.luv2code.springdemo.entity.Residential;
import com.luv2code.springdemo.entity.TableOption;

import com.luv2code.springdemo.entity.UserCheck;

@Repository
public class ResidentialDAOImpl implements ResidentialDAO {

	public static String forSaleOrRent;
	public static int lowerPriceLimit;
	public static int upperPriceLimit;
	public static int rooms;
	public static int bathrooms;
	public static Date dateOfPublish;

	public void set(TableOption tableOption) {

		forSaleOrRent = tableOption.getForsaleOrRent();
		System.out.println("Inja daram Sale or rent ro mikhunam: " + forSaleOrRent);
		lowerPriceLimit = tableOption.getLowerPriceLimit();
		System.out.println("Lower price is : " + lowerPriceLimit);
		upperPriceLimit = tableOption.getUpperPriceLimit();
		System.out.println("Upper price is : " + upperPriceLimit);
		rooms = tableOption.getRooms();
		System.out.println("rooms is : " + rooms);
		bathrooms = tableOption.getBathrooms();
		System.out.println("bathrooms is : " + bathrooms);
		dateOfPublish = tableOption.getDateOfPublish();
		System.out.println("The date is : " + dateOfPublish);
	}

	String q = " ";

	public String queryBuilderForSaleOrRent() {

		switch (forSaleOrRent) {

		case "sale": {
			q = "from Residential r where r.saleOrRent='" + forSaleOrRent + "' and r.price>='" + lowerPriceLimit
					+ "' and r.price<='" + upperPriceLimit + "' and r.rooms>='" + rooms + "'" + " and r.bathrooms>='"
					+ bathrooms + "' and r.dateOfPublish >'" + dateOfPublish + "'";

		}
			break;
		case "rent": {
			q = "from Residential r where r.saleOrRent='" + forSaleOrRent + "' and r.price>='" + lowerPriceLimit
					+ "' and r.price<='" + upperPriceLimit + "' and r.rooms>='" + rooms + "'" + " and r.bathrooms>='"
					+ bathrooms + "'";

		}
			break;

		default: {
			q = "from Residential";
		}
			break;
		}

		return q;
	}

	// need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	// @Transactional
	public List<Residential> getResidentials() {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query
		Query<Residential> theQuery = currentSession.createQuery(queryBuilderForSaleOrRent(), Residential.class);

		// execute query and get result list
		List<Residential> residentials = theQuery.getResultList();

		// return the results
		return residentials;
	}

	@Override
	// @Transactional
	public List<Residential> getAllResidentials() {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query
		Query<Residential> theQuery = currentSession.createQuery("from Residential", Residential.class);

		// execute query and get result list
		List<Residential> residentials = theQuery.getResultList();

		// return the results
		return residentials;
	}

	// @Override
	// //@Transactional
	// public List<Residential> getResidentials() {
	//
	// String id = "2";
	// Residential residential=new Residential();
	// // get the current hibernate session
	// Session currentSession = sessionFactory.getCurrentSession();
	// residential=currentSession.get(Residential.class,id);
	// System.out.println(residential);
	//
	//
	// return null;
	// }

	@Override
	public void saveProperty(Residential theProperty) {

		if (theProperty.getOptions().equals("residentialOption")) {
			Session currentSession = sessionFactory.getCurrentSession();
			currentSession.saveOrUpdate(theProperty);
		}

		if (theProperty.getOptions().equals("recreationalOption")) {
			Session currentSession = sessionFactory.getCurrentSession();

			Recreational theRecreational = new Recreational();

			theRecreational = convertToRecreational(theProperty);

			currentSession.saveOrUpdate(theRecreational);
		}

	}

	public Recreational convertToRecreational(Residential theProperty) {

		Recreational theRecreational = new Recreational();

		theRecreational.setId(theProperty.getId());
		theRecreational.setBathrooms(theProperty.getBathrooms());
		theRecreational.setBuildingType(theProperty.getBuildingType());
		theRecreational.setDateOfPublish(theProperty.getDateOfPublish());
		theRecreational.setImage(theProperty.getImage());
		theRecreational.setOptions(theProperty.getOptions());
		theRecreational.setPrice(theProperty.getPrice());
		theRecreational.setRooms(theProperty.getRooms());
		theRecreational.setSaleOrRent(theProperty.getSaleOrRent());
		theRecreational.setSummary(theProperty.getSummary());
		theRecreational.setTitle(theProperty.getTitle());

		return theRecreational;
	}

	@Override
	public Residential getProperty(int theId) {

		Session currentSession = sessionFactory.getCurrentSession();

		Residential theProperty = currentSession.get(Residential.class, theId);

		return theProperty;

	}

	@Override
	public void deleteProperty(int theId) {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// delete object with primary key
		Query theQuery = currentSession.createQuery("delete from Residential where id=:propertyId");
		theQuery.setParameter("propertyId", theId);

		theQuery.executeUpdate();
	}
	
	
	
	@Override
	public List<UserCheck> checkValidUser(UserCheck user) {

		// get current hibernate session
				Session currentSession = sessionFactory.getCurrentSession();
				Query<UserCheck> theQuery = currentSession.createQuery("from UserCheck",UserCheck.class);
                List<UserCheck> userCheck=theQuery.getResultList();
                List<UserCheck> test = new ArrayList<UserCheck>();

                for(UserCheck item: userCheck) {
                	if( user.getName().equals(item.getName())) {
                		if(user.getPassword().equals(item.getPassword())) {
                            test.add(item);
                            return test;
                			
                		}

                		else {
                			test.add(new UserCheck());
                			return  test;
                		}
				    }
                }
                test.add(new UserCheck());
    			return  test;
    			}

}
