package com.luv2code.springdemo.dao;

import java.sql.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.luv2code.springdemo.entity.Recreational;
import com.luv2code.springdemo.entity.Residential;
import com.luv2code.springdemo.entity.TableOption;

@Repository
public class RecreationalDAOImpl implements RecreationalDAO {

	// need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	// @Transactional
	public List<Recreational> getRecreationals() {

		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query
		Query<Recreational> theQuery = currentSession.createQuery("from Recreational", Recreational.class);

		// execute query and get result list
		List<Recreational> recreationals = theQuery.getResultList();

		// return the results
		return recreationals;
	}

	

	
	
	@Override
	public Recreational getProperty(int theId) {

		Session currentSession = sessionFactory.getCurrentSession();

		Recreational theProperty = currentSession.get(Recreational.class, theId);

		return theProperty;

	}



	@Override
	public void deleteProperty(int theId) {
		 // get the current hibernate session
		 Session currentSession = sessionFactory.getCurrentSession();
		
		 // delete object with primary key
		 Query theQuery =
		 currentSession.createQuery("delete from Recreational where id=:propertyId");
		 theQuery.setParameter("propertyId", theId);
		
		 theQuery.executeUpdate();
		
	}

}
