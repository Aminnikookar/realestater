package com.luv2code.springdemo.dao;

import java.util.List;

import com.luv2code.springdemo.entity.Recreational;
import com.luv2code.springdemo.entity.Residential;
import com.luv2code.springdemo.entity.TableOption;

public interface RecreationalDAO {

//	public List<Residential> getResidentials();

//	public List<Recreational> getRecreationals();

//	public void set(TableOption tableOption);

//	public void saveProperty(Residential theProperty);

//	public List<Residential> getAllResidentials();

	public Recreational getProperty(int theId);

	public void deleteProperty(int theId);

	List<Recreational> getRecreationals();

	//List<Recreational> getRecreationals();

	//public Residential getProperty(int theId);

//	public void deleteCustomer(int theId);
	
}
