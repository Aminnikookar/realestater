package com.luv2code.springdemo.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.luv2code.springdemo.dao.ResidentialDAO;
import com.luv2code.springdemo.dao.ResidentialDAOImpl;
import com.luv2code.springdemo.entity.Recreational;
import com.luv2code.springdemo.entity.Residential;
import com.luv2code.springdemo.entity.TableOption;
import com.luv2code.springdemo.service.ResidentialService;

import com.luv2code.springdemo.entity.UserCheck;

@Controller
@RequestMapping("/residential")
public class RealestaterController {

	@Autowired
	private ResidentialDAO residentialDAO;

	@Autowired
	private ResidentialService residentialService;

	public static String dropDownListResult;

	List<UserCheck> checkResult;
	
	String newsImageName;

	@GetMapping("/login")
	public String login(@ModelAttribute("user") UserCheck user) {

		return "login";
	}

	@RequestMapping(value = "/checkUser", produces = "text/plain;charset=UTF-8")
	public String checkUser(@ModelAttribute("user") UserCheck user) {

		checkResult = residentialService.checkValidUser(user);
		// System.out.println(checkResult.getAccessability());

		return "redirect:/residential/showAll";
	}

	@RequestMapping("/showForm")
	public String showForm(Model theModel) {

		// create a new student object
		TableOption tableOption = new TableOption();

		// add student object to the model attribute
		theModel.addAttribute("tableOption", tableOption);

		return "choosing";

	}

	@RequestMapping("/processForm")
	// @GetMapping("/processForm")
	public String processForm(Model theModel) {

		switch (dropDownListResult) {

		case "residentialOption": {
			List<Residential> theResidentials = residentialService.getResidentials();
			theModel.addAttribute("residential", theResidentials);

		}
			break;
		case "recreationalOption": {
			List<Recreational> theRecreationals = residentialService.getRecreationals();
			theModel.addAttribute("residential", theRecreationals);
		}
			break;
		default:
			break;
		}

		// if(dropDownListResult.equals("residentialOption")) {
		// List<Residential> theResidentials = residentialService.getResidentials();
		// theModel.addAttribute("residential", theResidentials);
		// }
		//
		// if(dropDownListResult.equals("recreationalOption")) {
		// List<Recreational> theRecreationals = residentialService.getRecreationals();
		// theModel.addAttribute("residential", theRecreationals);
		// }

		return "list";
	}

	@RequestMapping("/process")
	// @GetMapping("/processForm")
	public String processForm(@ModelAttribute("tableOption") TableOption tableOption) {
		// log the input data
		System.out.println("the option is : " + tableOption.getOptions());
		dropDownListResult = tableOption.getOptions();

		System.out.println("the sale or rent is : " + tableOption.getForsaleOrRent());

		// here I`m transferring my data to DAOImp
		residentialDAO.set(tableOption);

		return "redirect:/residential/processForm";
	}

	@RequestMapping("/console")
	public String console(Model theModel) {

		// Residential residential = new Residential();

		// theModel.addAttribute("residential", residential);

		TableOption tableOption = new TableOption();

		theModel.addAttribute("residential", tableOption);

		return "console";

	}

	public static String options;

	@RequestMapping("/saveProperty")
	public String saveCustomer(@ModelAttribute("residential") Residential theProperty) {

		// options = theProperty.getOptions();
		// System.out.println("the option is : " + options);

		residentialService.saveProperty(theProperty);

		return "console";
	}

	@RequestMapping("/showAll")
	public String showAll(Model theModel) {

		for (UserCheck item : checkResult) {
			if (!item.getAccessability().equals("this is null")) {

				List<Residential> theResidentials = residentialService.getAllResidentials();
				theModel.addAttribute("residential", theResidentials);

				List<Recreational> theRecreationals = residentialService.getRecreationals();
				theModel.addAttribute("recreational", theRecreationals);

				return "listOfAllProperties";
			}

			else {
				return "redirect:/residential/login";
			}
		}
		return "redirect:/residential/login";

	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("propertyId") int theId, Model theModel) {

		Residential theProperty = residentialService.getProperty(theId);

		theModel.addAttribute("residential", theProperty);

		return "console";
	}

	@GetMapping("/showFormForUpdateRecreational")
	public String showFormForUpdateRecreational(@RequestParam("propertyId") int theId, Model theModel) {

		Recreational theProperty = residentialService.getPropertyRecreational(theId);

		theModel.addAttribute("residential", theProperty);

		return "console";
	}

	@GetMapping("/deleteResidential")
	public String deleteProperty(@RequestParam("propertyId") int theId) {

		// delete the property
		residentialService.deleteProperty(theId);

		return "redirect:/residential/showAll";
	}

	@GetMapping("/deleteRecreational")
	public String deletePropertyRecreational(@RequestParam("propertyId") int theId) {

		// delete the property
		residentialService.deletePropertyRecreational(theId);

		return "redirect:/residential/showAll";
	}

	@RequestMapping("/upload")
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			ServletFileUpload sf = new ServletFileUpload(new DiskFileItemFactory());
			List<FileItem> multiFiles = sf.parseRequest(request);
			String[] fileName = { "" };
			int j = 0, count = 0;
			for (FileItem item : multiFiles) {
				String lStr = item.getName();
				int i = lStr.length() - 1;
				outerloop: while (i > 0) {
					if (count == 3) {
						if (!fileName[j].equals("gpj")) {
							request.getSession().setAttribute("error", "Can not upload this file");
							count = 0;
							++j;
							break outerloop;
						}
					}
					if (!(lStr.charAt(i) == '\\')) {
						fileName[j] += lStr.charAt(i);
						--i;
						++count;
					} else
						break;
				}
				if (count != 0) {
					fileName[j] = new StringBuffer(fileName[j]).reverse().toString();
					item.write(new File("\\Users\\Arya\\eclipse-workspace2\\realestater\\WebContent\\resources\\image\\" + fileName[j]));
					count = 0;
					String message = fileName[j] + "Succesfully upload";
					newsImageName = fileName[j];
					++j;
					request.setAttribute("error", message);
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
		response.sendRedirect("console");

	}

}