package com.luv2code.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.luv2code.springdemo.dao.RecreationalDAO;
import com.luv2code.springdemo.dao.ResidentialDAO;
import com.luv2code.springdemo.entity.Recreational;
import com.luv2code.springdemo.entity.Residential;
import com.luv2code.springdemo.entity.UserCheck;

@Service
public class ResidentialServiceImpl implements ResidentialService {

	// need to inject customer dao
	@Autowired
	private ResidentialDAO residentialDAO;
	
	@Autowired
	private RecreationalDAO recreationalDAO;
	
	
	@Override
	@Transactional
	public List<Residential> getResidentials() {
		return residentialDAO.getResidentials();
	}

	@Override
	@Transactional
	public List<Recreational> getRecreationals() {
	return recreationalDAO.getRecreationals();
	}
	
	@Override
	@Transactional
	public void saveProperty(Residential theProperty) {
		residentialDAO.saveProperty(theProperty);
		
	}
	
	@Override
	@Transactional
	public List<Residential> getAllResidentials() {
		return residentialDAO.getAllResidentials();
	}

	
	@Override
	@Transactional
	public Residential getProperty(int theId) {
		
		
		return residentialDAO.getProperty(theId);
	}

	
	@Override
	@Transactional
	public Recreational getPropertyRecreational(int theId) {
		
		
		return recreationalDAO.getProperty(theId);
	}

	@Override
	@Transactional
	public void deleteProperty(int theId) {
		
		residentialDAO.deleteProperty(theId);
	}

	@Override
	@Transactional
	public void deletePropertyRecreational(int theId) {
		
		recreationalDAO.deleteProperty(theId);
		
	}

	@Override
	@Transactional
	public List<UserCheck> checkValidUser(UserCheck user) {
		
		return residentialDAO.checkValidUser(user);
	}
	


}





