package com.luv2code.springdemo.service;

import java.util.List;

import com.luv2code.springdemo.entity.Recreational;
import com.luv2code.springdemo.entity.Residential;

import com.luv2code.springdemo.entity.UserCheck;

public interface ResidentialService {

	public List<Residential> getResidentials();

	public List<Recreational> getRecreationals();

//
	//public Residential getProperty(int theId);
//
//	public void deleteCustomer(int theId);
//	

	public void saveProperty(Residential theProperty);

	public List<Residential> getAllResidentials();

	public Residential getProperty(int theId);

	public Recreational getPropertyRecreational(int theId);

	public void deleteProperty(int theId);

	public void deletePropertyRecreational(int theId);
	
	public List<UserCheck> checkValidUser(UserCheck user);




	

	


}
