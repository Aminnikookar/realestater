DROP DATABASE IF EXISTS `realestater`;
CREATE DATABASE IF NOT EXISTS `realestater` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `realestater`;

--
-- -------------------------------------------------------------------------
--
--
-- Table structure for table `user`
--
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `accessability` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

--
-- -------------------------------------------------------------------------
--
--
-- Table structure for table `residential`
--
DROP TABLE IF EXISTS `residential`;

CREATE TABLE `residential` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `buildingType` varchar(200) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `saleOrRent` varchar(200) DEFAULT NULL,
  `summary` text DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `dateOfPublish` DATE NULL,
  `options` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `postalCode` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

--
-- -------------------------------------------------------------------------
--
INSERT INTO `realestater`.`residential` (`title`, `image`, `buildingType`, `price`, `saleOrRent`, `summary`, `rooms`, `bathrooms`, `dateOfPublish`, `address`, `postalCode`, `city`, `options`) VALUES ('ipark', 'www.image.com', 'house', '100000', 'sale', 'best view', '1', '1', '2017-01-01', '2000 saint marc', 'H3H2N9', 'Montreal', 'residentialOption');
INSERT INTO `realestater`.`residential` (`title`, `image`, `buildingType`, `price`, `saleOrRent`, `summary`, `rooms`, `bathrooms`, `dateOfPublish`, `address`, `postalCode`, `city`, `options`) VALUES ('green House', 'www.image2.come', 'appartment', '200000', 'rent', 'landscape', '2', '3', '2017-02-02', '6630 saint mathiue', 'H3H2N5', 'Montreal', 'residentialOption');
INSERT INTO `realestater`.`residential` (`title`, `image`, `buildingType`, `price`, `saleOrRent`, `summary`, `rooms`, `bathrooms`, `dateOfPublish`, `address`, `postalCode`, `city`, `options`) VALUES ('khepel House', 'www.image3.come', 'appartment3', '300000', 'sale', 'landscape3', '4', '5', '2017-03-03', '5510 sherbrooke', 'H8H7N6', 'Montreal', 'residentialOption');


--
-- Table structure for table `recreational`
--
DROP TABLE IF EXISTS `recreational`;

CREATE TABLE `recreational` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `buildingType` varchar(200) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `saleOrRent` varchar(200) DEFAULT NULL,
  `summary` text DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `bathrooms` int(11) DEFAULT NULL,
  `dateOfPublish` DATE NULL,
  `options` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `postalCode` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

--
-- -------------------------------------------------------------------------
--
INSERT INTO `realestater`.`recreational` (`title`, `image`, `buildingType`, `price`, `saleOrRent`, `summary`, `rooms`, `bathrooms`, `dateOfPublish`, `address`, `postalCode`, `city`, `options`) VALUES ('ipark', 'www.image.com', 'house', '100000', 'sale', 'best view', '1', '1', '2017-01-01', '2000 saint marc', 'H3H2N9', 'Montreal', 'recreationalOption');
INSERT INTO `realestater`.`recreational` (`title`, `image`, `buildingType`, `price`, `saleOrRent`, `summary`, `rooms`, `bathrooms`, `dateOfPublish`, `address`, `postalCode`, `city`, `options`) VALUES ('green House', 'www.image2.come', 'appartment', '200000', 'rent', 'landscape', '2', '3', '2017-02-02', '6630 saint mathiue', 'H3H2N5', 'Montreal', 'recreationalOption');
INSERT INTO `realestater`.`recreational` (`title`, `image`, `buildingType`, `price`, `saleOrRent`, `summary`, `rooms`, `bathrooms`, `dateOfPublish`, `address`, `postalCode`, `city`, `options`) VALUES ('khepel House', 'www.image3.come', 'appartment3', '300000', 'sale', 'landscape3', '4', '5', '2017-03-03', '5510 sherbrooke', 'H8H7N6', 'Montreal', 'recreationalOption');






